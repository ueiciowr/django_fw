from django.contrib import admin
from .models import Cidades, Noticias, Postos, Precos
from .models import ImagensNoticia

# Register your models here.
admin.site.register(Noticias)
admin.site.register(ImagensNoticia)

# ─────────── Prova ───────────

admin.site.register(Cidades)
admin.site.register(Postos)
admin.site.register(Precos)
