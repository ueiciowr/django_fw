from django.db import models

# Create your models here.
class Noticias(models.Model):
  data = models.DateTimeField(verbose_name="Data de Publicação")
  titulo = models.CharField(max_length=100)
  texto = models.TextField()
  def __str__(self):
    return self.titulo
  class Meta:
    verbose_name = 'noticias'
    ordering = ['-data']

class ImagensNoticia(models.Model):
  noticia = models.ForeignKey(Noticias, on_delete=models.PROTECT)
  legenda = models.CharField(max_length=700)
  foto = models.ImageField(upload_to='images')
  def __str__(self):
    return self.legenda

# ────── Prova ──────
class Cidades(models.Model):
  nome = models.CharField(max_length=200, help_text='max. 200 caracteres')
  uf = models.CharField(max_length=4, help_text='max. 4 caracteres')
  cep = models.CharField(max_length=50, help_text='max. 50 caracteres')

  def __str__(self):
    return self.nome

class Postos(models.Model):
  cidade = models.ForeignKey(Cidades, on_delete=models.PROTECT)
  nome = models.CharField(max_length=200, help_text='max. 200 caracteres')
  endereco = models.TextField();
  bairro = models.TextField();
  bandeira = models.CharField(max_length=30, help_text='max. 30 caracteres');
  razao_social = models.TextField();
  cnpj = models.CharField(max_length=30);

  def __str__(self):
    return self.nome

class Precos(models.Model):
  posto = models.ForeignKey(Postos, on_delete=models.PROTECT)
  tipo_combustivel = models.TextField()
  data_coleta = models.TextField();
  preco_venda = models.CharField(max_length=30);

  def __str__(self):
    return self.tipo_combustivel
