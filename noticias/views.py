from django.shortcuts import render
from django.http import HttpResponse
from .models import Cidades, ImagensNoticia, Noticias, Postos, Precos

# Create your views here.
def index(request):
  lista_noticias = Noticias.objects.all()
  return render(request, 'listanoticias.html', {'noticias':
  lista_noticias})

def ler_noticias(request, id):
  news = Noticias.objects.get(id=id)
  imagens = ImagensNoticia.objects.all().filter(noticia=id)
  return render(request, 'readnews.html', {'noticia': news, 'imagens': imagens})

# ────── Prova ──────
def home(request):
  return render(request, 'index.html')

def cidades(request):
  cidades = Cidades.objects.all()
  return render(request, 'cidades/index.html', { 'cidades': cidades })

def postos(request):
  postos = Postos.objects.all()
  return render(request, 'postos/index.html', { 'postos': postos })

# def posto_city(request, id):
#   print(id)
#   posto = Postos.objects.get(id=id)
#   # imagens = ImagensNoticia.objects.all().filter(noticia=id)
#   cidade = Postos.objects.all().filter(cidade_id=id)
#   print(cidade)
#   return render(request, 'index.html')

def precos(request):
  precos = Precos.objects.all()
  return render(request, 'precos/index.html', { 'precos': precos })

# def reports_precos(request):
#   precos = Precos.objects.all()
#   return render(request, 'precos/index.html', { 'precos': precos })

# ──────────── REPORTS POSTOS ────────────
def reports_postos(request):
  cidades = Cidades.objects.all()
  # print(request.POST['postos_id'])
  return render(request, 'reports/cidades.html', { 'cidades': cidades })

def reports_posto(request, id):
  postos = Postos.objects.all().filter(cidade_id=id)
  cidade = Cidades.objects.get(id=id)
  return render(request, 'reports/postos_city.html', { 'postos': postos, 'cidade': cidade })

# ──────────── REPORTS PREÇOS ────────────
def reports_precos(request):
  postos = Postos.objects.all()
  return render(request, 'reports/postos.html', { 'postos': postos })

def reports_preco(request, id):
  precos = Precos.objects.all().filter(posto_id=id)
  postos = Postos.objects.get(id=id)
  return render(request, 'reports/posto_price.html', { 'precos': precos, 'postos': postos })
