Criar ambiente virtual

```bash
virtualenv ambientevirtual
```

Ativar ambiente virtual

```bash
source bin/activate
```

Instalar Django

```bash
pip install django
```

Criar site admin

```bash
django-admin startproject site
```

Criar site

```bash
django-admin startproject siteproject
```

Gerar migrations

```bash
python manage.py migrate --plan
```

Gerar migrations

```bash
python manage.py migrate --plan
python manage.py showmigrations --list | grep -v '\[X\]'
```

Iniciar servidor

```
python manage.py runserver
```

Criar application

```
python manage.py startapp noticias
python manage.py runserver 7000
```

Escrever models e gerar migrations partir da model

```
python manage.py makemigrations noticias
```

Gerar migrations

```
python manage.py migrate
```

Criar user admin

```
python manage.py createsuperuser
```
