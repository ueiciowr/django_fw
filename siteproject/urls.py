"""siteproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from noticias import views

urlpatterns = [
    path('', views.home, name="home"),
    path('admin/', admin.site.urls),
    path('noticias/', views.index),
    path('noticias/<int:id>/', views.ler_noticias, name="news"),
    
    # Private routers
    path('cidades/', views.cidades),
    path('postos/', views.postos),
    path('precos/', views.precos),
    

    # Public routers
    #path('reports/precos/'),
    path('reports/postos/', views.reports_postos),
    path('reports/postos/<int:id>/', views.reports_posto, name="cidade"),
    path('reports/precos/', views.reports_precos),
    path('reports/precos/<int:id>/', views.reports_preco, name="posto"),
]
